#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export PATH=$PATH:/home/jacomago/bin/
alias ls='ls --color=auto'
complete -cf sudo
#PS1='\[\e[1m\]┌─[\u@\h][\W]\n\[\e[1m\]└─[\$]\[\e[0m\] '
#PS1="\[\e[01;31m\]┌─[\[\e[01;35m\u\e[01;31m\]]──[\[\e[00;37m\]${HOSTNAME%%.*}\[\e[01;32m\]]:\w$\[\e[01;31m\]\n\[\e[01;31m\]└──\[\e[01;36m\]>>\[\e[0m\]"
if [[ -f "$HOME/ps1" ]]
then
  export PROMPT_COMMAND="source $HOME/ps1"
fi
export ALTERNATE_EDITOR=""
fortune -e | cowsay -f kitty -b
#archey
export PYTHONDOCS=/usr/share/doc/python/html/

export EDITOR='nano'

export PERL_LOCAL_LIB_ROOT="/home/jacomago/perl5";
export PERL_MB_OPT="--install_base /home/jacomago/perl5";
export PERL_MM_OPT="INSTALL_BASE=/home/jacomago/perl5";
export PERL5LIB="/home/jacomago/perl5/lib/perl5/x86_64-linux-thread-multi:/home/jacomago/perl5/lib/perl5";
export PATH="/home/jacomago/perl5/bin:$PATH";

export PATH=$PATH:~/.gem/ruby/1.9.1/bin/

